<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Information;
use App\Worship;
use App\WorshipAttendance;
use App\WorshipSession;
use Carbon\Carbon;
use Dompdf\Adapter\PDFLib;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\New_;
use PDF;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.dashboard.index');
    }

    public function indexIbadah()
    {
        $worship = Worship::with('session')->orderBy('date', 'DESC')->get();
        return view('admin.ibadah.index', compact('worship'));
    }

    public function sesi(Request $request)
    {
        $sesi = $request->sesi;
        $date = $request->tanggal;
        $worship = Worship::with('session')->where('date', $request->tanggal)->first();

        $attendances = array();
        $session = $worship->session->where('session', $request->sesi)->first();
        foreach ($session->attendance as $attendance) {
            array_push($attendances, array('chair_number' => $attendance->chair_number, 'name' => $attendance->attendance_name, 'id' => $attendance->id));
        }
        return view('admin.ibadah.detail', compact('attendances', 'session', 'sesi', 'date'));
    }

    public function sesiPdf(Request $request)
    {
        $sesi = $request->sesi;
        $date = $request->tanggal;
        $worship = Worship::with('session')->where('date', $request->tanggal)->first();

        $attendances = array();
        $session = $worship->session->where('session', $request->sesi)->first();
        foreach ($session->attendance as $attendance) {
            array_push($attendances, array('chair_number' => $attendance->chair_number, 'name' => $attendance->attendance_name, 'id' => $attendance->id));
        }
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::loadView('admin.ibadah.excel', compact('attendances', 'session', 'sesi', 'date', 'worship'))->setPaper('a4', 'landscape');
        // If you want to store the generated pdf to the server then you can use the store function
        // $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download('daftar-hadir-ibadah-'.date('d-F-Y', $worship->date).'-ke-'.$request->sesi.'.pdf'); 
        
        // return view('admin.ibadah.excel', compact('attendances', 'session', 'sesi', 'date', 'worship'));
    }

    public function updateSesi(Request $request)
    {
        $session = WorshipSession::find($request->session_id);
        $session->speakers = $request->pembicara;
        $session->sermon_title = $request->sermon_title;
        $session->sermon = $request->sermon;
        $session->sermon_verse = $request->sermon_verse;
        $session->mulai_ibadah = $request->mulai_ibadah;
        $session->selesai_ibadah = $request->selesai_ibadah;
        $session->updated_at = Carbon::now();
        $session->save();

        Session::flash('success_message', 'Sukses mengubah data sesi ibadah.');
        return redirect()->route('kharisma-admin-sesi-detail', ['tanggal' => $request->date, 'sesi' => $request->sesi]);
    }

    public function deleteJemaat(Request $request, $tanggal, $sesi, $id)
    {
        $session = WorshipAttendance::find($id);
        $session->deleted_at = Carbon::now();
        $session->save();

        Session::flash('success_message', 'Sukses menghapus pendaftar ibadah.');
        return redirect()->route('kharisma-admin-sesi-detail', ['tanggal' => $tanggal, 'sesi' => $sesi]);
    }

    public function tambahIbadah(Request $request)
    {
        DB::beginTransaction();
        try {

            $check = Worship::where('date', strtotime($request->date))->first();

            if ($check) {
                DB::rollBack();
                Session::flash('failed_message', 'Ibadah sudah tersedia pada jadwal yang dipilih.');
                return redirect()->route('kharisma-admin-sesi');
            }

            $worship = new Worship();
            $worship->date = strtotime($request->date);
            $worship->save();

            // Membuat sesi
            for ($i = 1; $i <= 3; $i++) {
                $worshipSession = new WorshipSession();
                $worshipSession->worship_id = $worship->id;
                $worshipSession->session = $i;
                $worshipSession->speakers = "-";
                $worshipSession->sermon_title = "-";
                $worshipSession->sermon = "-";
                $worshipSession->sermon_verse = "-";

                if ($i == 1) {
                    $mulai_ibadah = "07:00:00";
                    $selesai_ibadah = "09:00:00";
                }

                if ($i == 2) {
                    $mulai_ibadah = "10:00:00";
                    $selesai_ibadah = "12:00:00";
                }

                if ($i == 3) {
                    $mulai_ibadah = "18:00:00";
                    $selesai_ibadah = "20:00:00";
                }

                $worshipSession->mulai_ibadah = $mulai_ibadah;
                $worshipSession->selesai_ibadah = $selesai_ibadah;

                $worshipSession->save();
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed_message', $th->getMessage());
            return redirect()->route('kharisma-admin-sesi');
        }

        DB::commit();
        Session::flash('success_message', 'Sukses menghapus pendaftar ibadah.');
        return redirect()->route('kharisma-admin-sesi');
    }

    public function hapusIbadah(Request $request, $tanggal)
    {

        DB::beginTransaction();

        try {
            $worship = Worship::where('date', $tanggal)->first();
            if ($worship) {
                $worship->deleted_at = Carbon::now();
                $worship->save();

                $sessions = WorshipSession::where('worship_id', $worship->id)->get();
                foreach ($sessions as $session) {
                    WorshipAttendance::where('worship_session_id', $session->id)->delete();
                    $session->deleted_at = Carbon::now();
                    $session->save();
                }
            }

            DB::commit();
            Session::flash('success_message', 'Sukses menghapus pendaftar ibadah.');
            return redirect()->route('kharisma-admin-sesi');
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed_message', $th->getMessage());
            return redirect()->route('kharisma-admin-sesi');
        }
    }
}
