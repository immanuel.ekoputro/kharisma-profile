<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Information;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class InformationController extends Controller
{
    public function listInformation()
    {
        $infos = Information::get();
        return view('admin.information.index', compact('infos'));
    }

    public function tambahInformationPage()
    {
        return view('admin.information.add');
    }

    public function simpanInfo(Request $req)
    {
        DB::beginTransaction();

        try {
            $tambah = new Information();
            $tambah->title = $req->judul;
            $tambah->information = $req->isi_informasi;
            $tambah->created_by = Auth::user()->id;
            $tambah->save();
            
            DB::commit();
            Session::flash('success_message', 'Sukses mengubah data sesi ibadah.');
            return redirect()->route('kharisma-admin-information');
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed_message', $th->getMessage());
            return redirect()->route('kharisma-admin-information');
        }
    }
}
