<?php

namespace App\Http\Controllers\ibadah;

use App\Http\Controllers\Controller;
use App\Worship;
use App\WorshipAttendance;
use App\WorshipSession;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class IbadahController extends Controller
{
    public function index(Request $request)
    {
        
        $worships = Worship::with('session')->where('date', $request->d)->first();
        $date = $request->d;
        $status = ($worships) ? 1 : 0;
        return view('ibadah.daftar', compact('date', 'status', 'worships'));
    }

    public function pilihKursi(Request $request)
    {
        $sesi = $request->s;
        $date = $request->d;

        // Mengabil data Kebaktian berdasarkan tanggal
        $worshipID = Worship::with('session')->where('date', $request->d)->first();
        if (!$worshipID) {
            dd("Error No Worship");
        }

        // Mengambil data sesi pada Kebaktian yang dipilih
        $attendances = array();
        $worshipSession = $worshipID->session->where('session', $request->s)->first();
        foreach ($worshipSession->attendance as $attendance) {
            array_push($attendances, $attendance->chair_number);
        }

        return view('ibadah.pendaftaran', compact('sesi', 'date', 'attendances'));
    }

    public function submitKursi(Request $request)
    {
        DB::beginTransaction();

        try {
            // Mengabil data Kebaktian berdasarkan tanggal
            $worshipID = Worship::with('session')->where('date', $request->date)->first();
            if (!$worshipID) {
                dd("Error No Worship");
            }

            // Mengambil data sesi pada Kebaktian yang dipilih
            $arrayChair = array();
            $worshipSession = $worshipID->session->where('session', $request->sesi)->first();
            foreach ($worshipSession->attendance as $attendance) {
                array_push($arrayChair, $attendance->chair_number);
            }


            // Mengambil Nomor Kursi yang dipilih
            $chairNumber = array();
            foreach ($request->all() as $key => $req) {
                if (str_contains($key, 'testing-checkbox-')) {
                    if ($req == 'on') {
                        $temp = str_replace("testing-checkbox-", "", $key);
                        if (in_array($temp, $arrayChair)) {
                            DB::rollBack();
                            if (str_contains(URL::previous(), 'dashboard')) {
                                Session::flash('failed_message', 'Mohon maaf, kursi yang anda pilih sudah tidak tersedia. Mohon refresh halaman browser anda untuk melihat tampilan yang terbaru.');
                                return redirect()->route('kharisma-admin-sesi-detail', ['tanggal' => $request->date, 'sesi' => $request->sesi]);
                            } else {
                                Session::flash('failed_message', 'Mohon maaf, kursi yang anda pilih sudah tidak tersedia. Mohon refresh halaman browser anda untuk melihat tampilan yang terbaru.');
                                return redirect()->route('ibadah-daftar', ['d' => $request->date]);
                            }
                        }
                        array_push($chairNumber, $temp);
                        $temp = '';
                    }
                }
            }

            if (count($chairNumber) <= 0) {
                DB::rollBack();
                if (str_contains(URL::previous(), 'dashboard')) {
                    Session::flash('failed_message', 'Mohon memilih kursi terlebih dahulu.');
                    return redirect()->route('kharisma-admin-sesi-detail', ['tanggal' => $request->date, 'sesi' => $request->sesi]);
                } else {
                    Session::flash('failed_message', 'Mohon memilih kursi terlebih dahulu.');
                    return redirect()->route('ibadah-daftar', ['d' => $request->date]);
                }
            }

            foreach ($chairNumber as $chairKey => $number) {
                $attendance = new WorshipAttendance;
                $attendance->worship_session_id = $worshipSession->id;
                $attendance->attendance_name = $request->jemaat;
                $attendance->chair_number = $number;
                $attendance->save();
            }

            DB::commit();
            Session::flash('success_message', 'Sukses melakukan pendaftaran ibadah.');
        } catch (\Throwable $th) {
            DB::rollBack();
            Session::flash('failed_message', 'Mohon maaf terjadi kesalahan pada website, silahkan hubungi admin gereja untuk perbaikan. Terima kasih.');
        }

        if (str_contains(URL::previous(), 'dashboard')) {
            return redirect()->route('kharisma-admin-sesi-detail', ['tanggal' => $request->date, 'sesi' => $request->sesi]);
        } else {
            return redirect()->route('ibadah-daftar', ['d' => $request->date]);
        }
    }
}
