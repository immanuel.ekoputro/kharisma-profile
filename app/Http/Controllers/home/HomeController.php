<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Information;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){        
        $infos = Information::limit(3)->get();
        return view('home.homepage', compact('infos'));
    }
}
