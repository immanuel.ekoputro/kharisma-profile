<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorshipSession extends Model
{
    use SoftDeletes;
    protected $with = ['attendance'];
    
    public function attendance(){
    	return $this->hasMany("App\WorshipAttendance", 'worship_session_id', 'id');
    }
}
