<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Worship extends Model
{
    use SoftDeletes;

    public function session(){
    	return $this->hasMany("App\WorshipSession");
    }
}
