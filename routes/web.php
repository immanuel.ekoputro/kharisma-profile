<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'home\HomeController@index',
    'as' => 'kharisma-home'
]);

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    Route::get('/', [
        'uses' => 'admin\AdminController@index',
        'as' => 'kharisma-admin-home'
    ]);

    Route::group(['prefix' => 'ibadah', 'middleware' => 'admin'], function () {
        Route::get('/', [
            'uses' => 'admin\AdminController@indexIbadah',
            'as' => 'kharisma-admin-sesi'
        ]);

        Route::get('/hapus/{tanggal}', [
            'uses' => 'admin\AdminController@hapusIbadah',
            'as' => 'kharisma-hapus-ibadah'
        ]);
    
        Route::get('/{tanggal}/{sesi}', [
            'uses' => 'admin\AdminController@sesi',
            'as' => 'kharisma-admin-sesi-detail'
        ]);

        Route::get('/pdf/{tanggal}/{sesi}', [
            'uses' => 'admin\AdminController@sesiPdf',
            'as' => 'kharisma-admin-sesi-detail-pdf'
        ]);        

        Route::post('/update-sesi', [
            'uses' => 'admin\AdminController@updateSesi',
            'as' => 'kharisma-update-sesi'
        ]);

        Route::post('/tambah-ibadah', [
            'uses' => 'admin\AdminController@tambahIbadah',
            'as' => 'kharisma-add-ibadah'
        ]);
    });

    Route::group(['prefix' => 'information', 'middleware' => 'admin'], function () {
        Route::get('/', [
            'uses' => 'admin\InformationController@listInformation',
            'as' => 'kharisma-admin-information'
        ]);

        Route::get('/tambah', [
            'uses' => 'admin\InformationController@tambahInformationPage',
            'as' => 'kharisma-admin-information-add-page'
        ]);

        Route::post('/simpan-info', [
            'uses' => 'admin\InformationController@simpanInfo',
            'as' => 'kharisma-admin-information-add-save'
        ]);

        // Route::get('/hapus/{tanggal}', [
        //     'uses' => 'admin\InformationController@hapusIbadah',
        //     'as' => 'kharisma-hapus-ibadah'
        // ]);
    
        // Route::get('/{tanggal}/{sesi}', [
        //     'uses' => 'admin\InformationController@sesi',
        //     'as' => 'kharisma-admin-sesi-detail'
        // ]);

        // Route::post('/update-sesi', [
        //     'uses' => 'admin\InformationController@updateSesi',
        //     'as' => 'kharisma-update-sesi'
        // ]);

        // Route::post('/tambah-ibadah', [
        //     'uses' => 'admin\InformationController@tambahIbadah',
        //     'as' => 'kharisma-add-ibadah'
        // ]);
    });

    Route::group(['prefix' => 'jemaat-ibadah', 'middleware' => 'admin'], function(){
        Route::get('/{tanggal}/{sesi}/delete-jemaat/{id}', [
            'uses' => 'admin\AdminController@deleteJemaat',
            'as' => 'kharisma-delete-attendance'
        ]);
    });
});


Route::group(['prefix' => 'ibadah'], function () {
    Route::get('/daftar-ibadah', [
        'uses' => 'ibadah\IbadahController@index',
        'as' => 'ibadah-daftar'
    ]);

    Route::get('/pilih-kursi', [
        'uses' => 'ibadah\IbadahController@pilihKursi',
        'as' => 'ibadah-pilih-kursi'
    ]);
    
    Route::post('/simpan-kursi', [
        'uses' => 'ibadah\IbadahController@submitKursi',
        'as' => 'ibadah-simpan-kursi'
    ]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
