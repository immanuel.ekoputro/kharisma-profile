{{-- navbar section --}}
<nav class="px-3 navbar-expand-lg navbar-light bg-white py-3 bg--navbar--top">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <a href="#" class="pr-3 hrev--header"><i class="fab fa-facebook-f"></i></a>
        <a href="#" class="pr-3 hrev--header"><i class="fab fa-twitter"></i></a>
        <a href="#" class="pr-3 hrev--header"><i class="fab fa-instagram"></i></a>
      </div>
      {{-- <div class="col-6 text-right">
        <a href="#" class="mr-3 hrev--header"><i class="fas fa-envelope"></i><span class=""> <span class="px-1">:</span> gpiakharisma@gmail.com</span></a>
        <a href="#" class=" hrev--header"><i class="fas fa-phone-alt"></i><span class=""> <span class="px-1">:</span> +62 856 9440 3646</span></a>
      </div> --}}
    </div>
  </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top py-4 shadow">
  <div class="container">
    <a class="navbar-brand" href="{{ route('kharisma-home') }}">
      <img src="{{ asset("assets/images/logo-1.png") }}" height="50" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse ml-auto" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto font-weight-bold text-uppercase">
        {{-- <li class="nav-item mr-2 kharisma--link active">
          <a class="nav-link" href="#">Ringkasan Khotbah</span></a>
        </li>
        <li class="nav-item mr-2 kharisma--link dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Wadah <i class="fas fa-chevron-down"></i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item mr-2 kharisma--link dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Laporan Kas <i class="fas fa-chevron-down"></i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li> --}}
        <li class="nav-item">
          <a class="btn btn-danger px-4 font-weight-bold d-none d-sm-block {{ ($today == $nextSunday) ? 'disabled' : '' }}" href="{{ route('ibadah-daftar', ['d' => $nextSunday]) }}">Pendaftaran Ibadah Raya</a>
        </li>
      </ul>
    </div>
  </div>
</nav>