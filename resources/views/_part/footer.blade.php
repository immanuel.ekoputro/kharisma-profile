<footer class="bg--kharisma--2">
  <div class="container py-5">
    <div class="row">
      <div class="col-sm-3 col-12 mr-auto">
        <div class="row">
          <div class="col-12 pb-4">
            <img src="{{ asset("assets/images/logo-1.png") }}" height="50" alt="">
          </div>
          <div class="col-12 small">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dapibus dictum lectus fringilla commodo. Nam posuere blandit dolor, nec viverra augue imperdiet posuere. Pellentesque sagittis ac velit eu ornare. Pellentesque tristique a lectus sit amet hendrerit. Nam viverra ipsum ac eleifend mollis.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>