<nav id="sidebar">
  <div class="sidebar-header">
    <img src="http://echurch.local/assets/images/logo-1.png" class="w-100" alt="">
  </div>

  <ul class="list-unstyled components pt-0 border-0">
    {{-- <p class="text-center">Dashboard</p> --}}
    <li class="px-3 py-1 pt-2 {{ (Request::segment(2) == 'ibadah' ) ? 'active' : '' }}">
      <a href="{{ route('kharisma-admin-sesi') }}" class="btn {{ (Request::segment(2) == 'ibadah' ) ? 'btn-dark' : 'btn-light' }} px-4">Ibadah</a>
    </li>
    <li class="px-3 py-1 {{ (Request::segment(2) == 'information' ) ? 'active' : '' }}">
      <a href="{{ route('kharisma-admin-information') }}" class="btn {{ (Request::segment(2) == 'information' ) ? 'btn-dark' : 'btn-light' }} px-4">Information</a>
    </li>
  </ul>

  <ul class="list-unstyled CTAs">
    <li>
      <a href="{{ Route('kharisma-home') }}" class="download">Back to Website</a>
    </li>
  </ul>
</nav>