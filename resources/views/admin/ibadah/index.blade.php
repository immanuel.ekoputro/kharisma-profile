@extends('master_admin')
@section('content')
  @if (Session::has('success_message'))
    <div class="alert alert-success">
      {{ Session::get('success_message') }}
    </div>
  @endif
  @if (Session::has('failed_message'))
    <div class="alert alert-danger">
      {{ Session::get('failed_message') }}
    </div>
  @endif
  <div class="card border-0">
    <div class="card-body">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12 mb-4">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambahIbadah">
              <i class="fas fa-plus"></i> Tambah Ibadah
            </button>

            <!-- Modal -->
            <div class="modal fade" id="tambahIbadah" tabindex="-1" role="dialog" aria-labelledby="tambahIbadahTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <form action="{{ route('kharisma-add-ibadah') }}" method="post">
                    @csrf
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Tambah Ibadah</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <input class="datepicker form-control" name="date">
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            {{-- Content --}}
            <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th>Tanggal Ibadah</th>
                  <th>Sesi</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($worship as $worsh)
                  <tr>
                    <td>{{ date('l, d F Y', $worsh->date) }}</td>
                    <td>
                      @foreach ($worsh->session as $session)
                        <a href="{{ Route('kharisma-admin-sesi-detail', ['tanggal' => $worsh->date,'sesi' => $session->session]) }}" class="btn btn-warning">Sesi {{ $session->session }}</a>
                      @endforeach
                    </td>
                    <td>
                      <a href="{{ Route('kharisma-hapus-ibadah', ['tanggal' => $worsh->date]) }}" onclick="return confirm('Are you sure delete this data?');" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function () {
      $('#example').DataTable({
        "ordering": false
      });
      $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
      });
    });
  </script>
@endsection