@extends('master_admin')
@section('content')
@if (Session::has('success_message'))
<div class="alert alert-success">
  {{ Session::get('success_message') }}
</div>
@endif
@if (Session::has('failed_message'))
<div class="alert alert-danger">
  {{ Session::get('failed_message') }}
</div>
@endif
<div class="card border-0">
  <div class="card-body">

    {{-- Content --}}
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 mb-4">
          <a href="{{ route('kharisma-admin-sesi-detail-pdf', ["sesi" => $sesi, "tanggal" => $date]) }}"
            class="btn btn-warning float-right">Print PDF</a>
        </div>
        <div class="col-12">
          <form action="{{ route('kharisma-update-sesi') }}" method="POST">
            @csrf
            <input type="hidden" name="session_id" value="{{ $session->id }}">
            <input type="hidden" name="sesi" value="{{ $sesi }}">
            <input type="hidden" name="date" value="{{ $date }}">
            <div class="form-group row">
              <div class="col-sm-4 col-12">
                <label for="Pembicara">Pembicara</label>
              </div>
              <div class="col-sm-8 col-12">
                <input type="text" class="form-control" name="pembicara" value="{{ $session->speakers }}">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-4 col-12">
                <label for="Judul">Judul Khotbah</label>
              </div>
              <div class="col-sm-8 col-12">
                <input type="text" class="form-control" name="sermon_title" value="{{ $session->sermon_title }}">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-4 col-12">
                <label for="Sermon">Ringkasan Khotbah</label>
              </div>
              <div class="col-sm-8 col-12">
                <input type="text" class="form-control" name="sermon" value="{{ $session->sermon }}">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-4 col-12">
                <label for="ayat">Ayat Emas</label>
              </div>
              <div class="col-sm-8 col-12">
                <input type="text" class="form-control" name="sermon_verse" value="{{ $session->sermon_verse }}">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-4 col-12">
                <label for="ayat">Jam Mulai Ibadah</label>
              </div>
              <div class="col-sm-8 col-12">
                <input type="text" class="form-control timepicker" name="mulai_ibadah"
                  value="{{ $session->mulai_ibadah }}">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-4 col-12">
                <label for="ayat">Jam Selesai Ibadah</label>
              </div>
              <div class="col-sm-8 col-12">
                <input type="text" class="form-control timepicker" name="selesai_ibadah"
                  value="{{ $session->selesai_ibadah }}">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-12">
                <button type="submit" class="btn btn-primary float-right px-5">Simpan</button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-12 text-center bg-dark py-5 mb-4" style="color: white">
          <span class="my-5">Mimbar</span>
        </div>
        <div class="col-12">
          <form action="{{ route('ibadah-simpan-kursi') }}" method="POST">
            <div class="row">
              <div class="col-12 table-responsive">
                @csrf
                <input type="hidden" name="sesi" value="{{ $sesi }}">
                <input type="hidden" name="date" value="{{ $date }}">
                <table id="rowClick" class="text-center w-100" style="height: 15rem">
                  <tbody>
                    @php
                    $num = 0
                    @endphp
                    @for ($i = 0; $i < 6; $i++) <tr>
                      <td colspan="10"
                        style="border-top: 1px solid white; border-left: 1px solid white; border-right: 1px solid white;"
                        class="text-left font-weight-bold">Baris Ke {{ $i+1 }}</td>
                      </tr>
                      <tr>
                        @for ($j = 0; $j < 10; $j++) @if (in_array($num, array_column($attendances, 'chair_number' )))
                          <td id="{{ $num }}" class="disabled--pendaftaran"
                          style="height: 5rem; width:10%; border: solid 2px black" name="testing-{{ $num }}">
                          {{ $j + 1 }}
                          <br>
                          {{$attendances[ array_search($num, array_column($attendances, 'chair_number')) ]['name']}}
                          <br>
                          <a type="button"
                            href="{{ route('kharisma-delete-attendance', ['tanggal' => $date, 'sesi' => $sesi, 'id' => $attendances[ array_search($num, array_column($attendances, 'chair_number')) ]['id'] ]) }}"
                            onclick="return confirm('Are you sure delete this data?');" class="btn btn-sm btn-danger"><i
                              class="fas fa-trash"></i></a>
                          <input type="checkbox" type="button" name="testing-checkbox-{{ $num }}"
                            id="testing-checkbox-{{ $num }}" hidden disabled>
                          </td>
                          @else
                          <td id="{{ $num }}" class="haha" style="height: 5rem; width:10%; border: solid 2px black"
                            name="testing-{{ $num }}">
                            {{ $j + 1 }}
                            <input type="checkbox" name="testing-checkbox-{{ $num }}" id="testing-checkbox-{{ $num }}"
                              hidden>
                          </td>
                          @endif
                          @php
                          $num++
                          @endphp
                          @endfor
                      </tr>
                      @endfor
                  </tbody>
                </table>
              </div>
              <div class="col-12">
                <div class="row">
                  <div class="col-sm-6 col-12">
                    <input type="text" name="jemaat" class="mt-3 w-100" placeholder="Penanggung Jawab Jemaat" required>
                    <small id="passwordHelpBlock" class="form-text text-muted">
                      * Penanggung jawab merupakan nama Kepala keluarga / atau Nama jemaat yang bertanggung jawab atas
                      daftar kehadiran yang telah dibuat.
                    </small>
                  </div>
                  <div class="col-sm-6 col-12">
                    <button type="submit" class="btn btn-danger px-5 float-right mt-3">Daftar</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function(){
      var chairSelected = 0;
      $(".haha").click(function(){

        if ( $("#testing-checkbox-"+this.id).prop('checked') )
        {
          if(chairSelected > 0)
          {
            chairSelected--;
          }

          $(this).toggleClass("active");
          $("#testing-checkbox-"+this.id).prop('checked', false);
        }
        else
        {
          $(this).toggleClass("active");
          chairSelected++;
          $("#testing-checkbox-"+this.id).prop('checked', true);
        }
      });

      $('.timepicker').timepicker({
          timeFormat: 'HH:mm',
          interval: 60,
          startTime: '0:00',
          dynamic: false,
          dropdown: true,
          scrollbar: true
      });
    });

    function deleteAttendance(){
      alert('delete');
    }
</script>
@endsection