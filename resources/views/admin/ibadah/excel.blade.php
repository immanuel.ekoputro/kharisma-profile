@extends('master_admin_root')
@section('content')
<h5 class="text-center">Ibadah Tanggal - <strong>{{ date('d F Y', $worship->date) }}</strong></h5>
<table id="rowClick" class="text-center w-100">
  <tbody>
    @php
        $num = 0
    @endphp
    @for ($i = 0; $i < 6; $i++)
    <tr>
      <td colspan="10" style="border-top: 1px solid white; border-left: 1px solid white; border-right: 1px solid white;" class="text-left font-weight-bold">Baris Ke {{ $i+1 }}</td>
    </tr>
    <tr>
      @for ($j = 0; $j < 10; $j++)
        @if (in_array($num, array_column($attendances, 'chair_number')))
          <td id="{{ $num }}" class="disabled--pendaftaran" style="height: 3rem; width:10%; border: solid 2px black" name="testing-{{ $num }}">
            {{ $j + 1 }}
            <br>
            <strong>{{$attendances[ array_search($num, array_column($attendances, 'chair_number')) ]['name']}}</strong>
          </td>
        @else
          <td id="{{ $num }}" class="haha" style="height: 3rem; width:10%; border: solid 2px black" name="testing-{{ $num }}">
            {{ $j + 1 }}
            <input type="checkbox" name="testing-checkbox-{{ $num }}" id="testing-checkbox-{{ $num }}" hidden>
          </td>
        @endif
      @php
          $num++
      @endphp
      @endfor
    </tr>
    @endfor
  </tbody>
</table>
@endsection

@section('script')
@endsection