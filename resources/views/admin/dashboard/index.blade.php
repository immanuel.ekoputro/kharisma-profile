@extends('master_admin')
@section('content')
  @if (Session::has('success_message'))
    <div class="alert alert-success">
      {{ Session::get('success_message') }}
    </div>
  @endif
  @if (Session::has('failed_message'))
    <div class="alert alert-danger">
      {{ Session::get('failed_message') }}
    </div>
  @endif
  <div class="card border-0">
    <div class="card-body">
      <div class="container-fluid">
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function () {
    });
  </script>
@endsection