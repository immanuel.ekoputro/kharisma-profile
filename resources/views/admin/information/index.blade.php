@extends('master_admin')
@section('content')
  @if (Session::has('success_message'))
    <div class="alert alert-success">
      {{ Session::get('success_message') }}
    </div>
  @endif
  @if (Session::has('failed_message'))
    <div class="alert alert-danger">
      {{ Session::get('failed_message') }}
    </div>
  @endif
  <div class="card border-0">
    <div class="card-body">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Button trigger modal -->
            <a href="{{ route('kharisma-admin-information-add-page') }}" class="btn btn-primary mb-4">
              <i class="fas fa-plus"></i> Tambah Ibadah
            </a>
            {{-- Content --}}
            <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th>Judul</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($infos as $info)
                  <tr>
                    <td>
                      {{ $info->title }}
                    </td>
                    <td>
                      {{-- <a href="{{ Route('kharisma-hapus-ibadah', ['tanggal' => $worsh->date]) }}" onclick="return confirm('Are you sure delete this data?');" class="btn btn-danger"><i class="fas fa-trash"></i></a> --}}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function () {
      $('#example').DataTable({
        "ordering": false
      });
      $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
      });
    });
  </script>
@endsection