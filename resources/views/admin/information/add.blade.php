@extends('master_admin')
@section('content')
  @if (Session::has('success_message'))
  <div class="alert alert-success">
    {{ Session::get('success_message') }}
  </div>
  @endif @if (Session::has('failed_message'))
  <div class="alert alert-danger">
    {{ Session::get('failed_message') }}
  </div>
  @endif
  <div class="card border-0">
    <div class="card-body">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <h3 class="">Tambah Informasi</h3>
            <hr>
            {{-- Content --}}
            <form action="{{ route('kharisma-admin-information-add-save') }}" method="POST">
              @csrf
              <div class="form-group row">
                <label for="judulInformasi" class="col-sm-2 col-form-label">Judul</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="judulInformasi" name="judul" />
                </div>
              </div>
              <div class="form-group row">
                <label for="informationContent" class="col-sm-2 col-form-label">Informasi</label>
                <div class="col-sm-10">
                  <textarea name="isi_informasi" id="informationContent" rows="10" class="form-control"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-12">
                  <button type="submit" class="btn btn-primary float-right px-5">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function () {
      $('#example').DataTable({
        "ordering": false
      });
      $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
      });
    });
  </script>

  <script>
    tinymce.init({
      selector: '#informationContent',
      menubar: false,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code'
      ],
      toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | code',
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css']
    });
  </script>
@endsection