<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Fontawesome Kit -->
  <script src="https://kit.fontawesome.com/6dfe3deb36.js" crossorigin="anonymous"></script>
  <!-- Google Font Kit -->
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700;900&display=swap" rel="stylesheet">
  <title>GPIA Kharisma - Official Website</title>
  <style>
    body {
      font-family: 'Source Sans Pro', sans-serif;
    }

    .carousel--height{
      height: 83.5vh;
    }

    .dropdown-toggle::after {
      display: none;
    }

    .bg--kharisma {
      background-color: #6F6B6A;
      color: #f7f7f7;
    }

    .bg--kharisma--2 {
      background-color: #c1c1c1;
      color: #6d6968;
    }

    .bg--navbar--top {
      background-color: #cecece !important;
    }

    .shadow{
      -webkit-box-shadow: 0px 7px 13px -1px rgba(0,0,0,0.28);
      -moz-box-shadow: 0px 7px 13px -1px rgba(0,0,0,0.28);
      box-shadow: 0px 7px 13px -1px rgba(0,0,0,0.28);
    }

    .shadow--2 {
      -webkit-box-shadow: 0px 7px 13px -1px rgba(0,0,0,0.5);
      -moz-box-shadow: 0px 7px 13px -1px rgba(0,0,0,0.5);
      box-shadow: 0px 7px 13px -1px rgba(0,0,0,0.5);
    }


    /* mouse over link */
    .kharisma--link > a:hover {
      color: red;
    }

    /* selected link */
    .kharisma--link > a:active {
      color: yellow;
    }

    .py-7 {
      padding-top: 7rem;
      padding-bottom: 7rem;
    }

    .mw-50 {
      max-width: 50rem;
    }

    .text-welcome {
      letter-spacing: 3px;
    }

    .text-subscribe {

    }

    .text-welcome-desc {
      line-height: 2rem;
    }

    .card--kharisma {
      
    }

    .card--kharisma--information {
      background-color: #b6b6b4;
    }

    .img--card--kharisma {
      border-radius: 0.5rem;
      -webkit-box-shadow: 0px 0px 23px -5px rgba(0,0,0,0.3);
      -moz-box-shadow: 0px 0px 23px -5px rgba(0,0,0,0.3);
      box-shadow: 0px 0px 23px -5px rgba(0,0,0,0.3);
    }

    .card--footer--kharisma {
      background-color: transparent !important;
      border: none;
      padding-left: 0;
    }

    .font-weight-700 {
      font-weight: 700;
    }

    .under--card--title{
      
    }

    .bg--yel {
      background-color: #ffe000;
    }

    .bg-f7 {
      background-color: #f7f7f7;
    }

    .parallax--kharisma {
      
      background-image: linear-gradient(
          rgba(0, 0, 0, 0.5), 
          rgba(0, 0, 0, 0.5)
        ), url("https://img.freepik.com/free-photo/cross-blurry-sunset-background_34982-10.jpg?size=626&ext=jpg");
      background-attachment: fixed;
      background-size: cover;
    }

    .date--sermon {
      z-index: 1003;
      border-radius: 10px;
      position: absolute;
      top: 12rem;
      right: 1rem;
    }

    .date--sermon--month {
      top: -10px;
      position: relative;
    }

    .gallery--kharisma{
      background-size: cover;
      background-position: center;
    transition: all 1s;
    }

    .gallery--kharisma:hover {
      transform: scale(1.2);
      filter: blur(3px);
      -webkit-filter: blur(3px);
    }

    table {
      border-collapse: collapse;
    }

    table, th, td {
      border: 1px solid black;
    }

    table tr td.active {background: #333; color: white}

    .cl--kharisma {
      color: #6d6968;
    }

    .hrev--header {
      color: #6d6968;
    }

    .hrev--header:hover {
      color: #3e3e3e;
      text-decoration: none;
    }

    .disabled--pendaftaran {
      background-color: #dcc100;
      color: #fff
    }

    @stack('style');

  </style>
</head>
  <body>
    <div id="app">
      @include('_part.navbar')

      @yield('content')
      
      @include('_part.footer')
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    
    @yield('script')
  </body>
</html>