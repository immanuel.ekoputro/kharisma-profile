@extends('master')

@section('content')
  @if ($status == 1)
  <div class="">
    <div class="bg--navbar--top">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg--navbar--top">
            <li class="breadcrumb-item"><a style="color:#dc3545" href="{{ route('kharisma-home') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pendaftaran Ibadah</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
  <div style="" class="pt-4 pb-5">
    <div class="container">
      @if (Session::has('success_message'))
          <div class="alert alert-success">
              {{ Session::get('success_message') }}
          </div>
      @endif
      @if (Session::has('failed_message'))
          <div class="alert alert-danger">
              {{ Session::get('failed_message') }}
          </div>
      @endif
      <h2 class="text-center pb-3">Ibadah Tanggal - <span class="font-weight-bold">{{ date('d F Y', $worships->date) }}</span></h2>
      <div class="card-deck">
        @foreach ($worships->session as $worship)
        <div class="card">
          <div class="card-body text-center">
            <h2>
              Ibadah {{ $worship->session }} Tersisa
            </h2>
            <h1 class="card-title m-0" style="font-size: 5rem">
              {{ 60 - count($worship->attendance) }}
            </h1>
            <h3 class="">
              Kursi
            </h3>
            <p class="card-text"><i class="far fa-clock pr-1"></i> {{ substr($worship->mulai_ibadah, 0, 5) }} - {{ substr($worship->selesai_ibadah, 0, 5) }}</p>
            <a href="{{ route('ibadah-pilih-kursi', ['s' => $worship->session, 'd' => $date]) }}" class="btn btn-warning font-weight-bold">Daftar Sekarang!</a>
          </div>
        </div>
        @endforeach
        {{-- <div class="card">
          <div class="card-body text-center">
            <h2>
              Ibadah 1 Tersisa
            </h2>
            <h1 class="card-title" style="font-size: 5rem">
              15
            </h1>
            <p class="card-text m-0"><small class="text-muted">Ibadah Pagi </small></p>
            <p class="card-text"><i class="far fa-clock pr-1"></i> 07:00 - 09:00</p>
            <a href="{{ route('ibadah-pilih-kursi', ['s' => 1, 'd' => $date]) }}" class="btn btn-warning font-weight-bold">Daftar Sekarang!</a>
          </div>
        </div>
        <div class="card">
          <div class="card-body text-center">
            <h2>
              Ibadah 2 Tersisa
            </h2>
            <h1 class="card-title" style="font-size: 5rem">
              15
            </h1>
            <p class="card-text m-0"><small class="text-muted">Ibadah Siang </small></p>
            <p class="card-text"><i class="far fa-clock pr-1"></i> 10:00 - 12:00</p>
            <a href="{{ route('ibadah-pilih-kursi', ['s' => 2, 'd' => $date]) }}" class="btn btn-warning font-weight-bold">Daftar Sekarang!</a>
          </div>
        </div>
        <div class="card">
          <div class="card-body text-center">
            <h2>
              Ibadah 3 Tersisa
            </h2>
            <h1 class="card-title" style="font-size: 5rem">
              15
            </h1>
            <p class="card-text m-0"><small class="text-muted">Ibadah Sore </small></p>
            <p class="card-text"><i class="far fa-clock pr-1"></i> 18:00 - 20:00</p>
            <a href="{{ route('ibadah-pilih-kursi', ['s' => 3, 'd' => $date]) }}" class="btn btn-warning font-weight-bold">Daftar Sekarang!</a>
          </div>
        </div> --}}
      </div>
    </div>
  </div>
  @else
  @include('extra.blank')
  @endif
  @include('_part.subscribe')
@endsection

@section('script')
  <script>
    $(document).ready(function(){
      $(".haha").click(function(){
        $(this).toggleClass("active");
        console.log(this.id)
        if ( $("#testing-checkbox-"+this.id).prop('checked') )
        {
          $("#testing-checkbox-"+this.id).prop('checked', false);
        }
        else
        {
          $("#testing-checkbox-"+this.id).prop('checked', true);
        }
      });
    });
  </script>
@endsection