@extends('master')

@section('content')
  <div class="py-7">
    <div class="container pb-3" style="height: 5rem">
      {{-- <div class="bg--kharisma mb-3 w-100" style="height: 5rem">
        
      </div> --}}
      <div class="h-100 bg--kharisma">
        <div class="row align-items-center h-100">
            <div class="mx-auto">
              MIMBAR
            </div>
        </div>
    </div>
    </div>
    <div class="container">
      <form action="{{ route('ibadah-simpan-kursi') }}" method="POST">
        <div class="row">
          <div class="col-12">
            @csrf
            <input type="hidden" name="sesi" value="{{ $sesi }}">
            <input type="hidden" name="date" value="{{ $date }}">
            <table id="rowClick" class="text-center w-100" style="height: 15rem">
              <tbody>
                @php
                    $num = 0
                @endphp
                @for ($i = 0; $i < 6; $i++)
                <tr>
                  <td colspan="10" style="border-top: 1px solid white; border-left: 1px solid white; border-right: 1px solid white;" class="text-left font-weight-bold">Baris Ke {{ $i+1 }}</td>
                </tr>
                <tr class="mb-3">
                  @for ($j = 0; $j < 10; $j++)
                    @if (in_array($num, $attendances))
                      <td id="{{ $num }}" class="disabled--pendaftaran" style="height: 2rem; width: 2rem" name="testing-{{ $num }}">
                        {{ $j+1 }}
                        <input type="checkbox" name="testing-checkbox-{{ $num }}" id="testing-checkbox-{{ $num }}" hidden disabled>
                      </td>
                    @else
                      <td id="{{ $num }}" class="haha" style="height: 2rem; width: 2rem" name="testing-{{ $num }}">
                        {{ $j+1 }}
                        <input type="checkbox" name="testing-checkbox-{{ $num }}" id="testing-checkbox-{{ $num }}" hidden>
                      </td>
                    @endif
                  @php
                      $num++
                  @endphp
                  @endfor
                </tr>
                @endfor
              </tbody>
            </table>
          </div>
          <div class="col-12">
            <div class="row">
              <div class="col-sm-6 col-12">
                <input type="text" name="jemaat" class="mt-3 w-100" placeholder="Penanggung Jawab Jemaat" required>
                <small id="passwordHelpBlock" class="form-text text-muted">
                  * Penanggung jawab merupakan nama Kepala keluarga / atau Nama jemaat yang bertanggung jawab atas daftar kehadiran yang telah dibuat.
                </small>
              </div>
              <div class="col-sm-6 col-12">
                <button type="submit" class="btn btn-danger px-5 float-right mt-3">Daftar</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('script')
  <script>
    $(document).ready(function(){
      var chairSelected = 0;
      $(".haha").click(function(){

        if ( $("#testing-checkbox-"+this.id).prop('checked') )
        {
          if(chairSelected > 0)
          {
            chairSelected--;
          }

          $(this).toggleClass("active");
          $("#testing-checkbox-"+this.id).prop('checked', false);
        }
        else
        {
          if (chairSelected >= 4) {
            alert("Mohon maaf, batas pemilihan kursi sudah melebihi batas maksimum!");
            return;
          }
          
          $(this).toggleClass("active");
          chairSelected++;
          $("#testing-checkbox-"+this.id).prop('checked', true);
        }
      });
    });
  </script>
@endsection