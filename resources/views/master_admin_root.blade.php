<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>GPIA Kharisma</title>

  <!-- Bootstrap CSS CDN -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
    integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

  
  <style>
    /*
        DEMO STYLE
    */

    @import "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700";

    body {
      font-family: 'Poppins', sans-serif;
      background: #fafafa;
    }

    /* ---------------------------------------------------
    CONTENT STYLE
----------------------------------------------------- */

    #content {
      width: calc(100% - 250px);
      padding: 40px;
      min-height: 100vh;
      transition: all 0.3s;
      position: absolute;
      top: 0;
      right: 0;
    }

    #content.active {
      width: 100%;
    }

    .card--kharisma {
      background-color: #b6b6b4;
    }

    .dataTables_filter {
      float: right;
    }

    .disabled--pendaftaran {
      background-color: #808080;
      color: #fff
    }

    table tr td.active {background: #333; color: white}

    table{border:none}
  </style>

</head>

<body>


  @yield('content')

</body>

</html>