@extends('master')
@section('content')
  {{-- carousel section --}}
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="{{asset('assets/images/gereja-1.jpg')}}" alt="First slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  
  <!-- Welcome Section -->
  <div class="container py-7">
    <div class="row">
      <div class="col-12 text-center">
        <h1 class="font-weight-bold text-welcome">SELAMAT DATANG DI GPIA KHARISMA</h1>
        <div class="mw-50 mt-4 mx-auto" >
          <h5 class="text-welcome-desc">Jalan Pinus 5 No. 2 Pondok Rejeki Kutabaru Tangerang (Tepat dibelakang, sekolah), Kuta Baru, PasarKemis, Kuta Baru, PasarKemis, Tangerang, Banten 15561, Indonesia</h5>
          <a class="btn btn-danger px-4 mt-4 font-weight-bold d-block d-sm-none {{ ($today == $nextSunday) ? 'disabled' : '' }}" href="{{ route('ibadah-daftar', ['d' => $nextSunday]) }}">Pendaftaran Ibadah Raya</a>
        </div>
      </div>
      {{-- <div class="col-12 pt-3">
        <div class="card-deck">
          <div class="card card--kharisma border-0">
            <img class="card-img-top img--card--kharisma" src="https://colorlib.com/preview/theme/crose/img/bg-img/3.jpg" alt="Card image cap">
            <div class="card-body bg-transparent pl-0 pt-4">
              <h5 class="card-title font-weight-700 card--title--kharisma">Card title</h5>
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              <a href="#">
                <small class="text-muted font-weight-bold">Read More<i class="fas fa-angle-right pl-2"></i><i class="fas fa-angle-right"></i><i class="fas fa-angle-right"></i></small>
              </a>
            </div>
          </div>
          <div class="card card--kharisma border-0">
            <img class="card-img-top img--card--kharisma" src="https://colorlib.com/preview/theme/crose/img/bg-img/3.jpg" alt="Card image cap">
            <div class="card-body bg-transparent pl-0 pt-4">
              <h5 class="card-title font-weight-700 card--title--kharisma">Card title</h5>
              <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
              <a href="#">
                <small class="text-muted font-weight-bold">Read More<i class="fas fa-angle-right pl-2"></i><i class="fas fa-angle-right"></i><i class="fas fa-angle-right"></i></small>
              </a>
            </div>
          </div>
          <div class="card card--kharisma border-0">
            <img class="card-img-top img--card--kharisma" src="https://colorlib.com/preview/theme/crose/img/bg-img/3.jpg" alt="Card image cap">
            <div class="card-body bg-transparent pl-0 pt-4">
              <h5 class="card-title font-weight-700 card--title--kharisma">Card title</h5>
              <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
              <a href="#">
                <small class="text-muted font-weight-bold">Read More<i class="fas fa-angle-right pl-2"></i><i class="fas fa-angle-right"></i><i class="fas fa-angle-right"></i></small>
              </a>
            </div>
          </div>
        </div>
      </div> --}}
    </div>
  </div>

  <!-- Jemaat Section -->
  {{-- <div class="text-center parallax--kharisma">
    <div class="container py-7 text-center">
      <h4 style="
      letter-spacing: 5px;
      color: #ffe000;
  ">A PLACE FOR YOU</h4>
      <h2 style="
      max-width: 50rem;
      line-height: 2.5rem;
      color: white;
  " class="font-weight-700 mx-auto pb-4 py-3">Find a place to connect and grow through a small group, class, or regular gathering.</h2>
      <button class="btn btn-primary">Become A Member</button>
    </div>
  </div> --}}

  {{-- Ringkasan Khotbah Section --}}
  {{-- <div class="container py-7">
    <div class="row">
      <div class="col-12 text-center">
        <h1 class="font-weight-bold text-welcome">LATEST SERMONS</h1>
        <div class="mw-50 mb-5 mt-4 mx-auto" >
          <h5 class="text-welcome-desc">A church isn't a building—it's the people. We meet in locations around the United States and globally at Life.Church Online. No matter where you join us.</h5>
        </div>
      </div>
      <div class="col-12 pt-3">
        <div class="card-deck">
          <div class="card card--kharisma border-0">
            <div class="date--sermon bg--kharisma px-3 py-2 text-center">
              <h1 class="font-weight-bold m-0">16</h1>
              <small class="date--sermon--month text-uppercase">March</small>
            </div>
            <img class="card-img-top img--card--kharisma" src="https://colorlib.com/preview/theme/crose/img/bg-img/3.jpg" alt="Card image cap">
            <div class="card-body bg-transparent pl-0 pt-4">
              <h5 class="card-title font-weight-700 card--title--kharisma mb-0">Card title</h5>
              <hr class="mt-1">
              <p class="mb-1"><i class="fas fa-user-tie mr-2"></i> Sermon From : Jorge Malone</p>
              <p class=""><i class="far fa-clock mr-2"></i>March 10 on 9:00 am - 11:00 am</p>
              <a href="#">
                <small class="text-muted font-weight-bold">Read More<i class="fas fa-angle-right pl-2"></i><i class="fas fa-angle-right"></i><i class="fas fa-angle-right"></i></small>
              </a>
            </div>
          </div>
          <div class="card card--kharisma border-0">
            <div class="date--sermon bg--kharisma px-3 py-2 text-center">
              <h1 class="font-weight-bold m-0">16</h1>
              <small class="date--sermon--month text-uppercase">March</small>
            </div>
            <img class="card-img-top img--card--kharisma" src="https://colorlib.com/preview/theme/crose/img/bg-img/3.jpg" alt="Card image cap">
            <div class="card-body bg-transparent pl-0 pt-4">
              <h5 class="card-title font-weight-700 card--title--kharisma mb-0">Card title</h5>
              <hr class="mt-1">
              <p class="mb-1"><i class="fas fa-user-tie mr-2"></i> Sermon From : Jorge Malone</p>
              <p class=""><i class="far fa-clock mr-2"></i>March 10 on 9:00 am - 11:00 am</p>
              <a href="#">
                <small class="text-muted font-weight-bold">Read More<i class="fas fa-angle-right pl-2"></i><i class="fas fa-angle-right"></i><i class="fas fa-angle-right"></i></small>
              </a>
            </div>
          </div>
          <div class="card card--kharisma border-0">
            <div class="date--sermon bg--kharisma px-3 py-2 text-center">
              <h1 class="font-weight-bold m-0">16</h1>
              <small class="date--sermon--month text-uppercase">March</small>
            </div>
            <img class="card-img-top img--card--kharisma" src="https://colorlib.com/preview/theme/crose/img/bg-img/3.jpg" alt="Card image cap">
            <div class="card-body bg-transparent pl-0 pt-4">
              <h5 class="card-title font-weight-700 card--title--kharisma mb-0">Card title</h5>
              <hr class="mt-1">
              <p class="mb-1"><i class="fas fa-user-tie mr-2"></i> Sermon From : Jorge Malone</p>
              <p class=""><i class="far fa-clock mr-2"></i>March 10 on 9:00 am - 11:00 am</p>
              <a href="#">
                <small class="text-muted font-weight-bold">Read More<i class="fas fa-angle-right pl-2"></i><i class="fas fa-angle-right"></i><i class="fas fa-angle-right"></i></small>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> --}}

  <!-- Jemaat Section -->
  {{-- <div class="text-center parallax--kharisma">
    <div class="container py-5 text-left">
      <h1 style="
      line-height: 2.5rem;
      color: #ffe000;" 
      class="font-weight-700">Markus 13:19</h1>
    <p style="
    color: white;">Karena pada masa itu akan terjadi kesusahan, seperti yang belum pernah terjadi sejak awal penciptaan yang diciptakan Tuhan sampai saat ini, dan yang tidak akan ada lagi.</p>
    </div>
  </div> --}}

  {{-- Information Section --}}
  {{-- <div class="container py-5">
    <div class="row">
      <div class="col-12 py-4">
        <h4 class="card-title font-weight-bold mb-1">
          <span class="bg-white pr-3 cl--kharisma">Ibadah Wadah Kembali aktif</span>
          <hr class="m-0" style="
            border: 3px solid rgb(109, 105, 104);
            position: relative;
            top: -11px;
            z-index: -1;
          ">
        </h4>
        <p class="card-text">Ibadah Wadah-wadah akan dimulai pada Bulan Juli 2020. Untuk anak-anak masih belum diizinkan untuk beribadah di Gedung Gereja, Oleh karenanya kami mengadakan Ibadah KharsimaKIDS secara Online setiap hari Minggu Pkl. 09.00 di Youtube GPIA Kharisma.</p>
        <p class="mb-1 small">From : Jorge Malone</p>
        <hr class="mt-1 shadow--2" style="border: 3px solid rgb(109, 105, 104)">
      </div>
      <div class="col-12 py-4">
        <h4 class="card-title font-weight-bold mb-1">
          <span class="bg-white pr-3 cl--kharisma">Iuran Kematian Mikaka</span>
          <hr class="m-0" style="
            border: 3px solid rgb(109, 105, 104);
            position: relative;
            top: -11px;
            z-index: -1;
          ">
        </h4>
        <p class="card-text">Bagi Peserta mikaka yang telah membayar iuran mikaka, kartu setornya dapat diminta kepada Pdm. Galvin Leonardo.</p>
        <p class="mb-1 small">From : Jorge Malone</p>
        <hr class="mt-1 shadow--2" style="border: 3px solid rgb(109, 105, 104)">
      </div>
      <div class="col-12 py-4">
        <h4 class="card-title font-weight-bold mb-1">
          <span class="bg-white pr-3 cl--kharisma">Janji Iman Natal Tahun 2020</span>
          <hr class="m-0" style="
            border: 3px solid rgb(109, 105, 104);
            position: relative;
            top: -11px;
            z-index: -1;
          ">
        </h4>
        <p class="card-text">Kembali Kita mengumpulkan Dana Natal Tahun 2020 melalui Janji Iman Natal. Isi formulir janji iman dan serahkan kepada Pdm. Galvin Leonardo</p>
        <p class="mb-1 small">From : Jorge Malone</p>
        <hr class="mt-1 shadow--2" style="border: 3px solid rgb(109, 105, 104)">
      </div>
    </div>
  </div> --}}

  {{-- Information Section 2 --}}
  <div class="py-7 bg-f7">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h1 class="font-weight-bold text-welcome">KHARISMA INFORMATION</h1>
          <div class="mw-50 mb-5 mt-4 mx-auto" >
            <h5 class="text-welcome-desc">A church isn't a building—it's the people. We meet in locations around the United States and globally at Life.Church Online. No matter where you join us.</h5>
          </div>
        </div>
        @foreach ($infos as $info)
          <div class="col-sm-6 col-12 py-4">
            <div class="p-3 h-100 bg--kharisma--2 rounded shadow">
              <h4 class="card-title font-weight-bold" style="position: absolute; top: 3px;">
                <span class="bg--kharisma--2 cl--kharisma px-2 py-1 rounded">{{ $info->title }}</span>
              </h4>
              <p class="card-text">{!! $info->information !!}</p>
              {{-- <p class="mb-1 small">From : Jorge Malone</p> --}}
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>

  {{-- Gallery Section --}}
  {{-- <div class="">
    <div class="card-group">
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
    </div> --}}
    {{-- <div class="card-group">
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
      <div class="card mb-0 border-0" style="
        height: 13rem; overflow: hidden; position: relative;">
        <div class="gallery--kharisma h-100" style="
        background-image: linear-gradient(
          rgba(255, 255, 255, 0.25), 
          rgba(255, 255, 255, 0.25)
        ),
        url('https://www.signupgenius.com/cms/socialMediaImages/25churchsmallgroup.jpg');">

        </div>
      </div>
    </div> --}}
  </div>

  @include('_part.subscribe')
@endsection

@section('script')
  <script>
    $(document).ready(function(){
    });
  </script>
@endsection