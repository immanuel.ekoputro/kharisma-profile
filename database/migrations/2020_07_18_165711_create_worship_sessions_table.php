<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorshipSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worship_sessions', function (Blueprint $table) {
            $table->id();
            $table->integer('worship_id')->unsigned();
            $table->string('session', 2);
            $table->string('speakers');
            $table->string('sermon_title');
            $table->text('sermon');
            $table->string('sermon_verse');
            $table->string('status', 2)->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worship_sessions');
    }
}
