<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMulaiIbadahAndSelesaiIbadahToWorshipSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('worship_sessions', function (Blueprint $table) {
            $table->time('mulai_ibadah', 0);
            $table->time('selesai_ibadah', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('worship_sessions', function (Blueprint $table) {
            $table->dropColumn('mulai_ibadah');
            $table->dropColumn('selesai_ibadah');
        });
    }
}
